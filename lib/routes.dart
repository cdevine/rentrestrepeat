import 'package:flutter/widgets.dart';
import 'package:rent_rest_repeat/screens/home_screen/home.dart';
import 'package:rent_rest_repeat/screens/splash.dart';
import 'package:rent_rest_repeat/screens/sign_in/sign_in.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  '/home': (BuildContext context) => HomeScreen(),
  '/login': (BuildContext context) => SignInPage(),
  '/splash': (BuildContext context) => SplashPage(),
};