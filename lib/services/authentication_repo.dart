import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class SignUpFailure implements Exception{}
class LoginWithEmailAndPasswordFailure implements Exception{}
class LoginWithGoogleFailure implements Exception{}
class LogoutFailure implements Exception{}
class UserUpdateFailure implements Exception{}

class AuthenticationRepository {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;

  AuthenticationRepository({
    FirebaseAuth firebaseAuth,
    GoogleSignIn googleSignIn})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
        _googleSignIn = googleSignIn ?? GoogleSignIn.standard();

  Stream<User> get user {
    return _firebaseAuth.authStateChanges().map((firebaseUser) {
      return firebaseUser;
    });
  }

  Future<void> signInWithGoogle() async{
    // Trigger the authentication flow
    try {
      final googleUser = await _googleSignIn.signIn();
      final googleAuth = await googleUser.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken
      );
      await _firebaseAuth.signInWithCredential(credential);
    } on Exception {
      throw LoginWithGoogleFailure();
    }
  }

  Future<void> signInWithEmailAndPassword({String email, String password}) async{
    assert(email != null && password != null);
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email,
          password: password,
      );

    } on Exception {
      throw LoginWithEmailAndPasswordFailure();
    }
  }

  //Sign in apis
  Future<void> signInAnonymously() async{
    FirebaseAuth.instance.signInAnonymously();
  }

  Future<void> registerWithEmailAndPassword({String email, String password}) async{
    assert(email != null && password != null);
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: email,
          password: password
      );
    } on FirebaseAuthException catch(e){
      throw e;
    }
  }

  Future<void> signOut() async{
    try {
      await Future.wait([
        _firebaseAuth.signOut(),
        _googleSignIn.signOut(),
      ]);
    } on Exception {
      throw LogoutFailure();
    }
  }

  Future<void> updateDisplayName(String displayName) async{
    try{
      await _firebaseAuth.currentUser.updateProfile(
        displayName: displayName
      );
    } on Exception {
      throw UserUpdateFailure();
    }
  }

  Future<void> updateProfile(String displayName, String photoUrl) async{
    try {
      await _firebaseAuth.currentUser.updateProfile(
          displayName: displayName,
        photoURL: photoUrl
      );
    } on Exception {
      throw UserUpdateFailure();
    }
  }
}