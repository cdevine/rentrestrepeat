import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rent_rest_repeat/blocs/auth/auth_bloc.dart';
import 'package:rent_rest_repeat/blocs/rentals/rentals_bloc.dart';
import 'package:rent_rest_repeat/routes.dart';
import 'file:///C:/Users/Cameron/AndroidStudioProjects/rent_rest_repeat/lib/screens/home_screen/view/home_screen.dart';
import 'package:rent_rest_repeat/screens/splash.dart';
import 'package:rent_rest_repeat/screens/sign_in/sign_in.dart';
import 'package:rent_rest_repeat/theme/style.dart';
import 'package:rentals_repository/rentals_repository.dart';
import 'package:user_repository/user_repository.dart';
class RentRestRepeatApp extends StatelessWidget {
  final _authRepository = FirebaseUserRepository();
  final _rentalsRepository = FirebaseRentalsRepository();

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<FirebaseUserRepository>(
          create: (context) => _authRepository,
        ),
        RepositoryProvider<RentalsRepository>(
            create: (context) => _rentalsRepository)
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (BuildContext context) => AuthBloc(_authRepository),
          ),
          /*BlocProvider(
            create: (BuildContext context) =>
                RentalsBloc(_rentalsRepository, context.read<AuthBloc>()),
          ),*/
        ],
        child: AppView(),
      ),
    );
  }
}

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => RentalsBloc(
            context.read<RentalsRepository>(), context.read<AuthBloc>()),
        child: MaterialApp(
          theme: ThemeData(primarySwatch: Colors.blue),
          navigatorKey: _navigatorKey,
          routes: routes,
          builder: (context, child) {
            return BlocListener<AuthBloc, AuthState>(
              listener: (context, state) {
                if (state is AuthAuthenticated) {
                  _navigator.pushAndRemoveUntil<void>(
                    HomeScreen.route(),
                    (route) => false,
                  );
                }
                if (state is AuthUnauthenticated) {
                  _navigator.pushAndRemoveUntil<void>(
                    SignInPage.route(),
                    (route) => false,
                  );
                }
              },
              child: child,
            );
          },
          onGenerateRoute: (_) => SplashPage.route(),
        ));
  }
}
