part of 'auth_bloc.dart';

abstract class AuthState extends Equatable{
  AuthState([List props = const[]]) : super();
}

/*
 * Unknown state
 */
class AuthUnknown extends AuthState {

  AuthUnknown() : super([]);

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'AuthStateUnknown';
  }
}

/*
 * Authenticated state
 */
class AuthAuthenticated extends AuthState{
  final User user;

  AuthAuthenticated(this.user) : super([user]);

  @override
  List<Object> get props => [user];

  @override
  String toString() {
    return 'AuthStateAuthenticated { user: $user }';
  }
}


/*
 * Unathenticated state
 */
class AuthUnauthenticated extends AuthState{
  final FirebaseAuthException exception;

  AuthUnauthenticated(this.exception) : super([exception]);

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'AuthStateUnauthenticated { exception : $exception }';
  }
}
