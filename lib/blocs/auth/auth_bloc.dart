import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:pedantic/pedantic.dart';
import 'package:user_repository/user_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState>{
  final FirebaseUserRepository authRepo;
  StreamSubscription<User> _userSubscription;

  AuthBloc(this.authRepo):
        assert(authRepo != null),
        super(AuthUnknown()){
    _userSubscription = authRepo.user.listen(
            (user) => add(AuthUserChanged(user))
    );
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if(event is AuthUserChanged){
      yield event.user != null ? AuthAuthenticated(event.user) : AuthUnauthenticated(null);
    }else if(event is AuthLogoutRequested){
      unawaited(authRepo.signOut());
    }else if(event is AuthLoginWithGoogleRequested){
      unawaited(authRepo.signInWithGoogle());
    }else if(event is AuthEmailLoginRequested){
      yield* _mapEmailLoginRequestToState(event);
    }
  }

  @override
  Future<void> close(){
    _userSubscription?.cancel();
    return super.close();
  }


  Stream<AuthState>_mapEmailLoginRequestToState(AuthEmailLoginRequested event) async*{
    String email = event.email;
    String password = event.password;
    print(email);
    print(password);
    try {
      authRepo.signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      yield AuthUnauthenticated(e);
    }
  }

}