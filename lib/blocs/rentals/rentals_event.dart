part of 'rentals_bloc.dart';

abstract class RentalsEvent extends Equatable{
  const RentalsEvent();

  @override
  List<Object> get props => [];
}

class LoadRentals extends RentalsEvent {}
class AddRental extends RentalsEvent {
  final Rental rental;

  const AddRental(this.rental);

  List<Object> get props => [rental];
}

class UpdateRental extends RentalsEvent {
  final Rental rental;

  const UpdateRental(this.rental);

  List<Object> get props => [rental];
}

class DeleteRental extends RentalsEvent {
  final Rental rental;

  const DeleteRental(this.rental);

  List<Object> get props => [rental];
}

class RentalsUpdated extends RentalsEvent {
  final List<Rental> rentals;
  const RentalsUpdated(this.rentals);

  @override
  List<Object> get props => [rentals];
}




