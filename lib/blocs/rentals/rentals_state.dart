part of 'rentals_bloc.dart';

@immutable
abstract class RentalsState extends Equatable{
  const RentalsState();

  @override
  List<Object> get props => [];
}

class RentalsInitial extends RentalsState {}

class RentalsLoading extends RentalsState {}

class RentalsLoaded extends RentalsState{
  final List<Rental> rentals;

  const RentalsLoaded(this.rentals);

  @override
  List<Object> get props => [rentals];
}

class RentalAddError extends RentalsState{
  final Rental addedRental;
  final error;

  const RentalAddError(this.addedRental, this.error);

  @override
  List<Object> get props => [addedRental,error];
}

class RentalUpdateError extends RentalsState{
  final Rental updatedRental;
  final error;

  const RentalUpdateError(this.updatedRental, this.error);

  @override
  List<Object> get props => [updatedRental,error];
}

class RentalDeleteError extends RentalsState{
  final error;

  const RentalDeleteError(this.error);

  @override
  List<Object> get props => [error];
}