import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:rentals_repository/rentals_repository.dart';
import 'package:rent_rest_repeat/blocs/auth/auth_bloc.dart';

part 'rentals_event.dart';
part 'rentals_state.dart';

class RentalsBloc extends Bloc<RentalsEvent, RentalsState> {
  final RentalsRepository _rentalsRepository;
  final AuthBloc _authBloc;
  StreamSubscription<List<Rental>> _rentalSubscription;
  StreamSubscription _authSubscription;

  RentalsBloc(this._rentalsRepository,this._authBloc) : super(RentalsInitial()){
    _authSubscription = _authBloc.listen((state) {
      if(state is AuthAuthenticated){
        log('Authenticated. Loading rentals');
        add(LoadRentals());
      }
    });
  }

  @override
  Stream<RentalsState> mapEventToState(
    RentalsEvent event,
  ) async* {
    if (event is LoadRentals) {
      yield* _mapLoadRentalsToState();
    }else if (event is RentalsUpdated) {
      yield* _mapRentalsUpdatedToState(event);
    } else if (event is AddRental) {
      yield* _mapAddRentalToState(event);
    } else if (event is UpdateRental) {
      yield* _mapUpdateRentalToState(event);
    } else if (event is DeleteRental) {
      yield* _mapDeleteRentalToState(event);
    }
  }

  @override
  Future<void> close(){
    _authSubscription?.cancel();
    return super.close();
  }

  Stream<RentalsState> _mapLoadRentalsToState() async* {
    _rentalSubscription?.cancel();
    _rentalSubscription = _rentalsRepository.rentals().listen((rentals) {
      log("Rentals loaded" + rentals.toString());
      add(RentalsUpdated(rentals));
    });
  }

  Stream<RentalsState> _mapRentalsUpdatedToState(RentalsUpdated event) async* {
    yield RentalsLoaded(event.rentals);
  }

  Stream<RentalsState> _mapAddRentalToState(AddRental event) async* {
    try {
      _rentalsRepository.addNewRental(event.rental);
    } catch (e) {
      yield RentalAddError(event.rental, e);
    }
  }

  Stream<RentalsState> _mapUpdateRentalToState(UpdateRental event) async* {
    try {
      _rentalsRepository.updateRental(event.rental);
    } catch (e){
      yield RentalUpdateError(event.rental, e);
    }
  }

  Stream<RentalsState> _mapDeleteRentalToState(DeleteRental event) async* {
    try {
      _rentalsRepository.deleteRental(event.rental);
    } catch (e){
      yield RentalDeleteError(e);
    }
  }
}
