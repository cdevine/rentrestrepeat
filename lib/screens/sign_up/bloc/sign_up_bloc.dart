import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rent_rest_repeat/services/authentication_repo.dart';

class SignUpFormBloc extends FormBloc<String, String>{
  final email = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
      FieldBlocValidators.email
    ],
  );

  final password = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
      FieldBlocValidators.passwordMin6Chars
    ],
  );

  final passwordConfirm = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
      FieldBlocValidators.passwordMin6Chars
    ],
  );

  final displayName = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
    ]
  );

  final AuthenticationRepository _authenticationRepository;

  @override
  void onSubmitting() async{
    //todo replace this with actual login
    emitSubmitting();
    try {
      await _authenticationRepository.registerWithEmailAndPassword(
          email: email.value,
          password: password.value);
      emitSuccess();
    } on FirebaseAuthException catch (e){
      if(e.code == 'email-already-in-use'){
        email.addFieldError('Account already exists with that email');
      }else if(e.code == 'invalid-email'){
        email.addFieldError('Invalid Email');
      }else if(e.code == 'weak-password'){
        password.addFieldError('Password not strong enough');
      }
      emitFailure(failureResponse: e.message);
    }
  }

  @override
  Future<void> close() async{
    super.close();
    displayName.close();
    email.close();
    password.close();
    passwordConfirm.close();
  }

  SignUpFormBloc(this._authenticationRepository){
    addFieldBlocs(
      fieldBlocs: [
        email,
        password,
        passwordConfirm,
        displayName,
      ],
    );

    passwordConfirm.addValidators([_confirmPassword(password)]);
    passwordConfirm.subscribeToFieldBlocs([password]);
  }

  Validator<String> _confirmPassword(TextFieldBloc passwordTextFieldBloc,){
    return(String confirmPassword){
      if(confirmPassword == passwordTextFieldBloc.value){
        return null;
      }
      return 'Must be equal to password';
    };
  }
}