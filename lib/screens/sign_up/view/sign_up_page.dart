import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rent_rest_repeat/services/authentication_repo.dart';
import 'package:rent_rest_repeat/screens/sign_up/sign_up.dart';

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthenticationRepository _authRepo =
    context.repository<AuthenticationRepository>();

    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      ),
      child: Container(
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.close,
                  size: 30.0,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),

            Container(
              padding: EdgeInsets.all(40),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Colors.blue,
                  boxShadow: [
                    new BoxShadow(
                        offset: new Offset(20, 10), blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Center(
                      child: Text('Sign Up', style: TextStyle(
                          fontSize: 48
                      ),),
                    ),
                    BlocProvider<SignUpFormBloc>(
                      create: (context) => SignUpFormBloc(_authRepo),
                      child: SignUpForm(),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        height: MediaQuery.of(context).size.height * 0.9,
        width: MediaQuery.of(context).size.width,
        color: Colors.deepOrange[100],
      ),
    );
  }
}
