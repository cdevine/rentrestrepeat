import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rent_rest_repeat/widgets/loading_dialog.dart';
import 'package:rent_rest_repeat/screens/sign_up/bloc/sign_up_bloc.dart';

class SignUpForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _signUpFormBloc = context.bloc<SignUpFormBloc>();
    final _focus = FocusScope.of(context);

    return FormBlocListener<SignUpFormBloc, String, String>(
      onSubmitting: (context, state) {
        LoadingDialog.show(context);
      },
      onSuccess: (context, state) {
        //LoadingDialog.hide(context);
        //Navigator.of(context).pop();
      },
      onFailure: (context, state) {
        LoadingDialog.hide(context);
      },
      child: Column(
        children: [
          TextFieldBlocBuilder(
            textFieldBloc: _signUpFormBloc.email,
            keyboardType: TextInputType.emailAddress,
            //textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Email',
              border: null,
              prefixIcon: Icon(Icons.email),
            ),

          ),
          SizedBox(
            height: 15,
          ),
          TextFieldBlocBuilder(
            textFieldBloc: _signUpFormBloc.password,
            suffixButton: SuffixButton.obscureText,
            //textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Password',
              prefixIcon: Icon(Icons.lock),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          TextFieldBlocBuilder(
            textFieldBloc: _signUpFormBloc.passwordConfirm,
            suffixButton: SuffixButton.obscureText,
            //textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Password Confirm',
              prefixIcon: Icon(Icons.lock),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          TextFieldBlocBuilder(
            textFieldBloc: _signUpFormBloc.displayName,
            //textInputAction: TextInputAction.done,
            onSubmitted: (_) => {_signUpFormBloc.submit()},
            decoration: InputDecoration(
              labelText: 'Display Name',
              prefixIcon: Icon(Icons.account_circle),
              //fillColor: Colors.red,
              //filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          RaisedButton(
            onPressed: _signUpFormBloc.submit,
            child: Text('Register'),
          )
        ],
      ),
    );
  }
}