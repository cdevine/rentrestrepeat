import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rent_rest_repeat/blocs/rentals/rentals_bloc.dart';
import 'package:rentals_repository/rentals_repository.dart';
import 'package:us_states/us_states.dart';

class AddRentalFormBloc extends FormBloc<String, String>{
  final RentalsBloc _rentalsBloc;
  final address1 = TextFieldBloc(
    validators: [
      FieldBlocValidators.required
    ],
  );

  final address2 = TextFieldBloc();

  final city = TextFieldBloc(
    validators: [
      FieldBlocValidators.required
    ],
  );

  final homeState = SelectFieldBloc(
    items: USStates.getAllNames(),
      validators: [
        FieldBlocValidators.required,
      ],

  );

  final zip = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
      _fiveDigitZip
    ],
  );

  @override
  void onSubmitting() async{
    Rental rental = new Rental(null, address1.value, address2.value, city.value, zip.value, homeState.value);
    _rentalsBloc.add(UpdateRental(rental));
    emitSuccess();
  }

  @override
  Future<void> close() async{
    super.close();
    address1.close();
    address2.close();
    city.close();
    homeState.close();
    zip.close();
  }

  AddRentalFormBloc(this._rentalsBloc){
    addFieldBlocs(
      fieldBlocs: [
        address1,
        address2,
        city,
        homeState,
        zip
      ],
    );
  }

  static String _fiveDigitZip(String zip){
    RegExp exp = new RegExp(r'^\d{5}$');
    if(!exp.hasMatch(zip)){
      return 'Zip Code is invalid';
    }
    return null;
  }
}