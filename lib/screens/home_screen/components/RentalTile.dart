import 'dart:math';

import 'package:flutter/material.dart';
import 'package:rentals_repository/rentals_repository.dart';

class RentalTile extends StatelessWidget {
  final Rental _rental;

  RentalTile(this._rental);

  @override
  Widget build(BuildContext context) {
    return GridTile(
      child: Container(
        color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        child: Column(
          children: [
            Expanded(
              child: FittedBox(
                fit: BoxFit.fill,
                child: Icon(
                  Icons.home_rounded,
                  color:
                      Colors.accents[Random().nextInt(Colors.accents.length)],
                ),
              ),
            ),
          ],
        ),
      ),
      footer: GridTileBar(
        title: Text(_rental.address['address1']),
        subtitle:
            Text(_rental.address['city'] + ', ' + _rental.address['state']),
        backgroundColor: Colors.black45,
      ),
    );
  }
}
