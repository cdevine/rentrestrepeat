import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rent_rest_repeat/blocs/rentals/rentals_bloc.dart';
import 'package:rent_rest_repeat/screens/home_screen/components/RentalTile.dart';
import 'package:rent_rest_repeat/widgets/loading_dialog.dart';
import 'package:rentals_repository/rentals_repository.dart';

class RentalsGridView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RentalsBloc, RentalsState>(builder: (context, state) {
      if(state is RentalsLoaded){
        return new OrientationBuilder(
          builder: (context,orientation) {
            return GridView.count(
              children: _buildGridTileList(state.rentals),
              crossAxisCount: orientation == Orientation.portrait ? 2 : 4,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              padding: EdgeInsets.all(5.0),
            );
          },
        );
      } else {
        return LoadingDialog();
      }
    });

  }
  
  List<RentalTile>_buildGridTileList(List<Rental> rentals) =>
    List.generate(10, (index) => RentalTile(rentals[0]));
}
