import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rent_rest_repeat/blocs/auth/auth_bloc.dart';
import 'package:rent_rest_repeat/screens/home_screen/components/RentalsGridView.dart';

class HomeScreen extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => HomeScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Home Page"),),
      body: Container(
        child: RentalsGridView(),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Text("Sign Out"),
              onTap: (){
                context.read<AuthBloc>().add(AuthLogoutRequested());
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {},
      ),
    );
  }

  void signOut(){

  }
}
