import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rent_rest_repeat/blocs/auth/auth_bloc.dart';

class LoginFormBloc extends FormBloc<String, String> {
  final AuthBloc _authBloc;
  final email = TextFieldBloc(
    validators: [FieldBlocValidators.required, FieldBlocValidators.email],
  );

  final password = TextFieldBloc(
    validators: [FieldBlocValidators.required],
  );

  @override
  void onSubmitting() async {
    _authBloc.add(
        AuthEmailLoginRequested(email: email.value, password: password.value));
  }

  @override
  Future<void> close() async {
    super.close();
    email.close();
    password.close();
  }

  LoginFormBloc(this._authBloc) {
    addFieldBlocs(
      fieldBlocs: [
        email,
        password,
      ],
    );
  }
}
