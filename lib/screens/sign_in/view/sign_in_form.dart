import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:rent_rest_repeat/blocs/auth/auth_bloc.dart';
import 'package:rent_rest_repeat/widgets/loading_dialog.dart';
import 'package:rent_rest_repeat/screens/sign_in/bloc/sign_in_bloc.dart';

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginFormBloc>(
        create: (context) => LoginFormBloc(context.read<AuthBloc>()),
        child: _LoginForm());
  }
}

class _LoginForm extends StatelessWidget {
  _LoginForm();

  @override
  Widget build(BuildContext context) {
    final _focus = FocusNode();
    return Container(
      child: FormBlocListener<LoginFormBloc, String, String>(
        onSubmitting: (context, state) {
          LoadingDialog.show(context);
        },
        onSuccess: (context, state) {
          //LoadingDialog.hide(context);
        },
        onFailure: (context, state) {
          LoadingDialog.hide(context);
        },
        child: Column(
          children: [
            TextFieldBlocBuilder(
              textFieldBloc: context.select((LoginFormBloc form) => form.email),
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onSubmitted: (_) => FocusScope.of(context).requestFocus(_focus),
              decoration: InputDecoration(
                  labelText: 'Email',
                  prefixIcon: Icon(Icons.email),
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)))),
            ),
            TextFieldBlocBuilder(
              textFieldBloc: context.select((LoginFormBloc form) => form.password),
              suffixButton: SuffixButton.obscureText,
              textInputAction: TextInputAction.done,
              focusNode: _focus,
              onSubmitted: (_) => FocusScope.of(context).unfocus(),
              decoration: InputDecoration(
                  labelText: 'Password',
                  fillColor: Colors.white,
                  filled: true,
                  prefixIcon: Icon(Icons.lock),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)))),
            ),
            SizedBox(
              height: 40,
            ),
            _loginButton(Buttons.Email, () {
              context.read<LoginFormBloc>().submit();
            }),
            SizedBox(
              height: 5,
            ),
            _loginButton(Buttons.GoogleDark, () {
              context.read<AuthBloc>().add(AuthLoginWithGoogleRequested());
            }),
          ],
        ),
      ),
    );
  }

  SignInButton _loginButton(Buttons button, void function()) {
    return SignInButton(
      button,
      onPressed: function,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
    );
  }
}
