import 'package:flutter/material.dart';
import 'package:rent_rest_repeat/screens/sign_in/sign_in.dart';
import 'package:rent_rest_repeat/screens/sign_up/sign_up.dart';

class SignInPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => SignInPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      backgroundColor: Colors.lightGreen[200],
      body: Center(
        child: Container(
          padding: EdgeInsets.all(40),
          child: Column(
            children: [
              SizedBox(
                  height: 225,
                  width: 225,
                  child: DecoratedBox(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          color: Colors.blue,
                          boxShadow: [
                            new BoxShadow(
                                offset: new Offset(20, 10), blurRadius: 20)
                          ],
                          gradient: new LinearGradient(
                              colors: [Colors.blue, Colors.deepPurple],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight)),
                      child: Icon(
                        Icons.home_work,
                        size: 200,
                        color: Colors.greenAccent,
                      ))),
              SizedBox(
                height: 35,
              ),
              LoginForm(),
              Expanded(
                  child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Don\'t have an account?',
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    TextButton(
                      child: Text(
                        'Sign Up',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      onPressed: () => {_registerSheet(context)},
                    ),
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  void _registerSheet(context) {
    showModalBottomSheet(
      context: context,
      enableDrag: true,
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: Colors.transparent,
      builder: (context) => SignUpPage(),
    );
  }
}
