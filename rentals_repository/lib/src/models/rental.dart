import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import '../entities/entities.dart';

@immutable
class Rental extends Equatable {
  final address;
  final String id;

  Rental(this.id, String address1, String address2, String city, String zip,
      String state)
      : this.address = {
          'address1': address1,
          'address2': address2,
          'city': city,
          'zip': zip,
          'state': state
        };

  @override
  List<Object> get props => [id, address];

  @override
  String toString(){
    return 'Rental: { ID: $id, Address: $address }';
  }

  RentalEntity toEntity() {
    return RentalEntity(id, address);
  }

  static Rental fromEntity(RentalEntity entity) {
    return Rental(
        entity.id, entity.address['address1'], entity.address['address2'],
        entity.address['city'], entity.address['zip'], entity.address['state'],
    );
  }
}
