import 'dart:async';
import 'package:rentals_repository/rentals_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rentals_repository/src/entities/entities.dart';

class FirebaseAddException extends FirebaseException {}

class FirebaseRentalsRepository implements RentalsRepository {
  final rentalCollection = FirebaseFirestore.instance.collection('rentals');

  @override
  Future<void> addNewRental(Rental rental) {
    return rentalCollection
        .add(rental.toEntity().toDocument())
        .then((value) => print('Rental Added'))
        .catchError((error) => throw error);
  }

  @override
  Future<void> deleteRental(Rental rental) {
    return rentalCollection
        .doc(rental.id)
        .delete()
        .then((value) => print('Rental Deleted'))
        .catchError((onError) => throw onError);
  }

  @override
  Future<void> updateRental(Rental rental) {
    return rentalCollection
        .doc(rental.id)
        .update(rental.toEntity().toDocument());
  }

  @override
  Stream<List<Rental>> rentals() {
    return rentalCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => Rental.fromEntity(RentalEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}
