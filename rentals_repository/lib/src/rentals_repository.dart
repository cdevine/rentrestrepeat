import 'dart:async';
import 'package:rentals_repository/rentals_repository.dart';

abstract class RentalsRepository{
  Future<void> addNewRental(Rental rental);

  Future<void> deleteRental(Rental rental);

  Future<void> updateRental(Rental rental);

  Stream<List<Rental>> rentals();
}