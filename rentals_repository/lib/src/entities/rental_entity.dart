import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class RentalEntity extends Equatable {
  final Map address;
  final String id;

  const RentalEntity(this.id, this.address);

  @override
  List<Object> get props => [address, id];

  @override
  String toString() {
    // TODO: implement toString
    return super.toString();
  }

  Map<String, Object> toJson() {
    return {
      'address' : address,
      'id': id
    };
  }

  static RentalEntity fromJson(Map<String, Object> json) {
    return RentalEntity(
      json['id'] as String,
      json['address'] as Map,
    );
  }

  static RentalEntity fromSnapshot(DocumentSnapshot snap) {
    Map<String, dynamic> data = snap.data();
    return RentalEntity(snap.id, data['address']);
  }

  Map<String, Object> toDocument() {
    return {
      'address': address
    };
  }
}
