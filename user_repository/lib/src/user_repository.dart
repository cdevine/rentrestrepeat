abstract class UserRepository {
  Future<void> signInWithGoogle();
  Future<void> signInWithEmailAndPassword();
  Future<void> register();
  Future<void> signOut();
  bool isAuthenticated();
  String getUserId();
}